package main

import (
	"flag"
	"fmt"

	"bitbucket.org/antizealot1337/dice"
)

func main() {
	// The number of sides of the die/dice to roll
	sides := flag.Int("sides", 6, "The number of sides for die")

	// The number of times to roll the dice
	times := flag.Int("times", 1, "The number of times to roll a die")

	// Determines if we should total the rolls at the end
	total := flag.Bool("total", false, "Total all the rolls or not")

	// Parse the command line argumentes
	flag.Parse()

	// The total for all the rolls
	var sum int

	// Loop the specified number of times
	for i := 0; i < *times; i++ {
		// Calculate the current roll
		roll := dice.Roll(dice.IntSides(*sides))

		// Check if we have more rolling to do
		if i != (*times)-1 {
			// Print the roll and a comman
			fmt.Printf("%d, ", roll)
		} else {
			// Print the roll and a new line
			fmt.Println(roll)
		} //if else

		// Check if we are totaling to rolls
		if *total {
			// Add the roll to the sum
			sum += roll
		} //if
	} //for

	// Check if we totalled the rolls
	if *total {
		fmt.Println("Total:", sum)
	} //if
} //main
