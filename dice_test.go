package dice

import "testing"

func TestIntSides(t *testing.T) {
	// Create a sides type
	sides := IntSides(3)

	// Make sure it is still equal to 3
	if sides != 3 {
		t.Error("Expected sides to be equal to 3 but was", sides)
	} //if
} //TestIntSides

func TestRoll(t *testing.T) {
	// Test the roll for 20 times with a 5 sided die.
	for i := 0; i < 5; i++ {
		// Roll a 2 sided die
		roll := Roll(5)

		// Check the roll
		if roll < 1 || roll > 5 {
			t.Error("Expected roll to be between 1 and 5 but was", roll)
		} //if
	} //for
} //TestRoll

func TestD2(t *testing.T) {
	// Test the roll 5 times for a D2 die
	for i := 0; i < 5; i++ {
		// Roll a 2 sided die
		roll := D2()

		// Check the roll
		if roll < 1 || roll > 2 {
			t.Error("Expected roll to be between 1 and 2 but was", roll)
		} //if
	} //for
} //TestD2

func TestD3(t *testing.T) {
	// Test the roll 10 times for a D3 die
	for i := 0; i < 10; i++ {
		// Roll a 3 sided die
		roll := D3()

		// Check the roll
		if roll < 1 || roll > 3 {
			t.Error("Expected roll to be between 1 and 3 but was", roll)
		} //if
	} //for
} //TestD3

func TestD4(t *testing.T) {
	// Test the roll 10 times for a D4 die
	for i := 0; i < 10; i++ {
		// Roll a 4 sided die
		roll := D4()

		// Check the roll
		if roll < 1 || roll > 4 {
			t.Error("Expected roll to be between 1 and 4 but was", roll)
		} //if
	} //for
} //TestD4

func TestD6(t *testing.T) {
	// Test the roll 12 times for a D6 die
	for i := 0; i < 12; i++ {
		// Roll a 6 sided die
		roll := D6()

		// Check the roll
		if roll < 1 || roll > 6 {
			t.Error("Expected roll to be between 1 and 6 but was", roll)
		} //if
	} //for
} //TestD6

func TestD8(t *testing.T) {
	// Test the roll 20 times for a D8 die
	for i := 0; i < 20; i++ {
		// Roll an 8 sided die
		roll := D8()

		// Check the roll
		if roll < 1 || roll > 8 {
			t.Error("Expected roll to be between 1 and 8 but was", roll)
		} //if
	} //for
} //TestD8

func TestD10(t *testing.T) {
	// Test the roll 20 times for a D10 die
	for i := 0; i < 20; i++ {
		// Roll an 10 sided die
		roll := D10()

		// Check the roll
		if roll < 1 || roll > 10 {
			t.Error("Expected roll to be between 1 and 10 but was", roll)
		} //if
	} //for
} //TestD10

func TestD12(t *testing.T) {
	// Test the roll 20 times for a D12 die
	for i := 0; i < 20; i++ {
		// Roll an 12 sided die
		roll := D12()

		// Check the roll
		if roll < 1 || roll > 12 {
			t.Error("Expected roll to be between 1 and 12 but was", roll)
		} //if
	} //for
} //TestD12

func TestD20(t *testing.T) {
	// Test the roll 40 times for a D20 die
	for i := 0; i < 40; i++ {
		// Roll an 20 sided die
		roll := D20()

		// Check the roll
		if roll < 1 || roll > 20 {
			t.Error("Expected roll to be between 1 and 20 but was", roll)
		} //if
	} //for
} //TestD20

func TestD100(t *testing.T) {
	// Test the roll 120 tiems for a D100 die
	for i := 0; i < 120; i++ {
		// Roll an 100 sided die
		roll := D100()

		// Check the roll
		if roll < 1 || roll > 100 {
			t.Error("Expected roll to be between 1 and 100 but was", roll)
		} //if
	} //for
} //TestD100
