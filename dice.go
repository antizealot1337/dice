// Package dice simulates rolling a die. It can roll any number of sided die or
// any of the convienice methods may be used.

package dice

import (
	"math/rand"
	"time"
)

func init() {
	// Seed the random number generation
	rand.Seed(time.Now().Unix())
} //init

// The number of sides for a die
type Sides uint32

// Converts an int value to sides
func IntSides(sides int) Sides {
	return Sides(sides)
} //Sides

// Rolls an n sided die. The result should be completely includive [1 - n]
func Roll(sides Sides) int {
	return rand.Intn(int(sides)) + 1
} //roll

// A convenience method. It is the same as calling Roll(2).
func D2() int {
	return Roll(2)
} //D2

// A convenience method. It is the same as calling Roll(3).
func D3() int {
	return Roll(3)
} //D3

// A convenience method. It is the same as calling Roll(4).
func D4() int {
	return Roll(4)
} //D4

// A convenience method. It is the same as calling Roll(6).
func D6() int {
	return Roll(6)
} //D6

// A convenience method. It is the same as calling Roll(8).
func D8() int {
	return Roll(8)
} //D8

// A convenience method. It is the same as calling Roll(10).
func D10() int {
	return Roll(10)
} //D10

// A convenience method. It is the same as calling Roll(12).
func D12() int {
	return Roll(12)
} //D12

// A convenience method. It is the same as calling Roll(20).
func D20() int {
	return Roll(20)
} //D20

// A convenience method. It is the same as calling Roll(100).
func D100() int {
	return Roll(100)
} //D100
