[![GoDoc](https://godoc.org/bitbucket.org/antizealot1337/dice?status.svg)](https://godoc.org/bitbucket.org/antizealot1337/dice)

#Dice

Dice simulates rolling an n-sided dice for the Go programing language. It is a
library and includes a command for simple command line rolling.

## Installation

To install run `go get bitbucket.org/antizealot1337/dice`. You can then import it
into your project and use any of the methods in the package. If you want to
install the command line application for use run
`go install bitbucket.org/antizealot1337/dice/cmd/roller`.

## Command line utility

The command line utility has 3 arguments that are `-sides`, `-times`, and
`-total`. They control the sides, number of times/rolls, and if the rolls are
totaled at the end respectively.

## License
Copyright 2015, 2016, 2019
Licensed under the terms of the Simplified BSD license. See license file for
terms.
